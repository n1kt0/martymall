from django import template
from cart import cart

register = template.Library()

@register.inclusion_tag("shop/cart/cart_box.html")

def cart_box(request):
	cart_item_count = cart.cart_item_count(request)
	cart_value = cart.cart_total_value(request)
	# currency = cart.get_currency(request)
	return {'cart_item_count': cart_item_count, 'cart_value':cart_value,}
