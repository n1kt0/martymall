# -*- coding: UTF-8 -*-
from django import template

register = template.Library()

@register.filter(name='pluralru')

def pluralru(value):
	if value in range(11,15):
		return u'ов'
	elif value == 1:
		return ''
	elif int(str(value)[-1]) in range(2,5):
		return u'а'
	elif int(str(value)[-1]) in range(5,10):
		return u'ов'
	elif int(str(value)[-1]) == 0:
		return u'ов'
	else:
		return ''
