from django.db import models
from shoplist.models import *
from mall import settings
import os

class Product_Category(models.Model):
    shop_slug = models.ForeignKey(Shops)
    name =  models.CharField(max_length=50)
    category_slug = models.SlugField(("Slug"), help_text=("Used for URLs"), blank=True)
    category_parent = models.ForeignKey('self', blank=True, null=True, related_name='child')
    meta = models.TextField(("Meta Description"), blank=True, null=True,
        help_text=("Meta description for this category"))
    description = models.TextField(("Description"), blank=True,
        help_text="Optional")
    ordering = models.IntegerField(("Ordering"), default=0, help_text=("Override alphabetical order in category display"))
    is_active = models.BooleanField(("Active"), default=True, blank=True)
    
    class Meta:
        verbose_name_plural = 'Product Categories'
    
    def __unicode__(self):
        return self.name
    
    @models.permalink
    def get_absolute_url(self):
        return ('category', (), {
            'slug': self.shop_slug,
            'category_slug': self.category_slug})

class Product(models.Model):
    shop_slug = models.ForeignKey(Shops)
    product_category = models.ManyToManyField(Product_Category, blank=True, verbose_name=("Category"))
    name = models.CharField(("Name"), max_length=255, blank=False)
    product_slug = models.SlugField(("Slug"), help_text=("Used for URLs"), blank=True)
    sku = models.CharField(("SKU"), max_length=255, blank=True, null=True)
    meta_keywords = models.TextField(("meta keywors"), max_length=200, default='', blank=True)
    meta_description = models.TextField(("meta description"), max_length=400, default='', blank=True)
    short_description = models.TextField(("Short description of product"), 
                                        help_text=("This should be a 1 or 2 line description in the default site language for use in product listing screens"), 
                                        max_length=200, default='', blank=True)
    description = models.TextField(("Description of product"), help_text=("This field can contain HTML and should be a few paragraphs in the default site language explaining the background of the product, and anything that would help the potential customer make their purchase."), 
                                    default='', blank=True)
    items_in_stock = models.DecimalField(("Qty on stock"), max_digits=18, decimal_places=6, default='0')
    date_added = models.DateField(("Date added"), null=True, blank=True)
    active = models.BooleanField(("Active"), default=True, help_text=("This will determine whether or not this product will appear on the site"))
    price = models.DecimalField(max_digits=14, decimal_places=6)
    featured = models.BooleanField(("Featured"), default=False, help_text=("Featured items will show on the front page"))
    ordering = models.IntegerField(("Ordering"), default=0, help_text=("Override alphabetical order in category display"))  
    total_sold = models.DecimalField(("Qty Sold"), max_digits=18, decimal_places=6, default='0')
    image = models.ImageField(upload_to='products/images/')
    
    def __unicode__(self):
        return self.name
    
    @models.permalink
    def get_absolute_url(self):
        return ('product', (), {
            'slug': self.shop_slug,
            'product_slug': self.product_slug})

class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name='images')
    image = models.ImageField(upload_to='products/images/')

class ProductAttributes(models.Model):
    product = models.ForeignKey(Product, related_name='attributes')
    attribute = models.CharField(("Attribute name"), max_length=45, blank=True)
    attribute_value = models.TextField(("Attribute value"), max_length=77, blank=True)