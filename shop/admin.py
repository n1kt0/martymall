from django.contrib import admin
from shop.forms import ProductAdminForm
from shop.models import *

class Product_CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'shop_slug', 'category_parent')
    list_display_links = ('name',)
    list_per_page = 20
    search_fields = ('name',)

    # sets up slug to be generated from category name
    prepopulated_fields = {'category_slug' : ('name',)}

class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 3

class ProductAttributesInline(admin.TabularInline):
    model = ProductAttributes

class ProductAdmin(admin.ModelAdmin):
    form = ProductAdminForm
    
    inlines = [ ProductImageInline,
                ProductAttributesInline,
    ]
    filter_horizontal = ('product_category',)
    list_display = ('name', 'shop_slug',)
    list_display_links = ('name',)
    list_per_page = 50
    search_fields = ('name',)

    # sets up slug to be generated from product name
    prepopulated_fields = {'product_slug' : ('name',)}

admin.site.register(Product_Category, Product_CategoryAdmin)
admin.site.register(Product, ProductAdmin)

