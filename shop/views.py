# -*- coding: UTF-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.core import urlresolvers
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from shoplist.models import *
from shop.models import *
from shop.forms import ProductAddToCartForm
from cart import cart


def shop_main(request, slug):
    if request.method == "POST":
        uname, pswd = request.POST['username'], request.POST['password']
        user = authenticate(username=uname, password=pswd)
        if user is not None:
            if user.is_active:
                login(request, user)
                url = request.META.get('HTTP_REFERER', None)
                return HttpResponseRedirect(url)
    else:
        shop_data = Shops.objects.get(slug=slug)
        shop_slug = shop_data.id
        categories = Product_Category.objects.filter(shop_slug=shop_slug)
        login_form = AuthenticationForm
        featured_products = Product.objects.filter(shop_slug=shop_slug, featured=1)
        featured_items = []
        for item in featured_products:
            featured_items.append((item.name, ProductImage.objects.filter(product=item.id)[0].image))
        return render_to_response('shop/base.html', RequestContext(request, {'shop_data':shop_data, 'categories':categories, 
                                                                            'featured_products':featured_items,
                                                                            'login':login_form, 'request':request}))
        # return render_to_response('shop/base.html', locals(), context_instance=RequestContext(request))

def category(request, slug, category_slug):
    if request.method == "POST":
        uname, pswd = request.POST['username'], request.POST['password']
        user = authenticate(username=uname, password=pswd)
        if user is not None:
            if user.is_active:
                login(request, user)
                url = request.META.get('HTTP_REFERER', None)
                return HttpResponseRedirect(url)
    else:
        shop_data = Shops.objects.get(slug=slug)
        shop_slug = shop_data.id
        categories = Product_Category.objects.filter(shop_slug=shop_slug)
        category_data = Product_Category.objects.get(category_slug=category_slug)
        category_products = Product.objects.filter(product_category=category_data.id)
        login_form = AuthenticationForm
        return render_to_response('shop/category.html', RequestContext(request, 
                                                                        {'shop_data':shop_data, 'categories':categories, 
                                                                        'category':category_data, 'products':category_products,
                                                                        'login':login_form, 'request':request,}))
    
def product(request, slug, product_slug):
    p = get_object_or_404(Product, product_slug=product_slug)
    if request.method == 'POST':
        post_data = request.POST.copy()
        form = ProductAddToCartForm(request, post_data)
        #check if posted data is valid
        if form.is_valid():
            #add to cart and redirect to cart page
            cart.add_to_cart(request)
        # if test cookie worked, get rid of it
        if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
        url = urlresolvers.reverse('show_cart')
        return HttpResponseRedirect(url)
    else:
        form = ProductAddToCartForm()
        shop_data = Shops.objects.get(slug=slug)
        product_data = Product.objects.get(product_slug = product_slug)
        shop_slug = shop_data.id
        categories = Product_Category.objects.filter(shop_slug=shop_slug)
        # assign the hidden input the product slug
        form.fields['product_slug'].widget.attrs['value'] = product_slug
        # set the test cookie on our first GET request
        request.session.set_test_cookie()
        images = ProductImage.objects.filter(product=product_data.id)
        login_form = AuthenticationForm
        return render_to_response('shop/product.html', RequestContext(request, {'shop_data':shop_data, 'product_data':product_data, 
                                                                                'images':images, 'form':form, 'login':login_form, 
                                                                                'categories':categories, 'request':request}))
