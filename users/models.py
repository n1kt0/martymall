from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	phone_num = models.CharField(("Phone number"), max_length=24)
	del_address = models.CharField(("Delivery address"), max_length=55)

