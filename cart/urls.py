from django.conf.urls.defaults import *

urlpatterns = patterns('cart.views', 
	(r'^$', 'show_cart', {'template_name':'shop/cart/cart.html'}, 'show_cart')
)