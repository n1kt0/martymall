# -*- coding: UTF-8 -*-
from django.shortcuts import render_to_response
from django.core import urlresolvers
from django.http import HttpResponseRedirect
from django.template import RequestContext
from shop.models import ProductImage
import cart

def show_cart(request, template_name="cart/cart.html"):
	if request.method == 'POST':
		post_data = request.POST.copy()
		if post_data.get('submit', '') == u'-':
			cart.delete_from_cart(request)
			url = urlresolvers.reverse('show_cart')
			return HttpResponseRedirect(url)
		elif post_data.get('submit', '') == u'Изменить':
			cart.update_qty(request)
			url = urlresolvers.reverse('show_cart')
			return HttpResponseRedirect(url)
	else:
		url = request.META.get('HTTP_HOST', None)
		cart_item_count = cart.cart_item_count(request)
		cart_items = cart.get_cart_items(request)
		title = 'Shopping Cart'
		cart_value = cart.cart_total_value(request)
		summary = []
		for each in cart_items:
			if len(each.name()) > 20:
				each.name = each.name()[:20]+'...'
			summary.append((ProductImage.objects.filter(product=each.product.id)[0].image, 
							each.name, 
							each.price, 
							each.quantity, 
							each.total,
							each.product_id,
							each.get_absolute_url,))
		return render_to_response(template_name, locals(),
			context_instance=RequestContext(request))

def checkout(request):
	return render_to_response("shop/checkout.html", locals(), context_instance=RequestContext(request))