from models import CartItem
from shop.models import Product, ProductImage
from shoplist.models import Shops
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
import decimal
import random

CART_ID_SESSION_KEY = 'cart_id'

# get current user's cart id, sets one if blank
def _cart_id(request):
	if request.session.get(CART_ID_SESSION_KEY, '') == '':
		request.session[CART_ID_SESSION_KEY] = _generate_cart_id()
	return request.session[CART_ID_SESSION_KEY]

def _generate_cart_id():
	cart_id = ''
	characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()'
	cart_id_length = 50
	for y in range(cart_id_length):
		cart_id += characters[random.randint(0, len(characters)-1)]
	return cart_id

# return all items for the current user's cart
def get_cart_items(request):
	return CartItem.objects.filter(cart_id=_cart_id(request))

# add an item to the cart
def add_to_cart(request):
	post_data = request.POST.copy()
	# get product slug from post data, return empty if blank
	product_slug = post_data.get('product_slug', '')
	# get product qty added, return 1 if empty
	quantity = post_data.get('quantity', 1)
	# fetch the product or return a missing page error
	p = get_object_or_404(Product, product_slug=product_slug)
	# get products is cart
	cart_products = get_cart_items(request)
	product_in_cart = False
	# check if items is already in cart
	for cart_item in cart_products:
		if cart_item.product.id == p.id:
			# updated the qty if found
			cart_item.augment_quantity(quantity)
			product_in_cart = True
	if not product_in_cart:
		# create and save new cart item
		ci = CartItem()
		ci.product = p
		ci.quantity = quantity
		ci.cart_id = _cart_id(request)
		ci.save()

# deletes items from cart
def delete_from_cart(request):
	post_data = request.POST.copy()
	product_id = post_data.get('item_id', '')
	item = CartItem.objects.get(cart_id=_cart_id(request), product_id=product_id)
	item.delete()

# updates qty in shopping cart
def update_qty(request):
	post_data = request.POST.copy()
	product_id = post_data.get('item_id', '')
	quantity = post_data.get('quantity', '')
	item = CartItem.objects.get(cart_id=_cart_id(request), product_id=product_id)
	if int(quantity) > 0:
		item.quantity = quantity
		item.save()
	else:
		item.delete()


# returns the total number of items in the user's cart
def cart_item_count(request):
	return get_cart_items(request).count()

def cart_total_value(request):
	items = get_cart_items(request)
	value = 0
	for item in items:
		value = value + item.total()
	return value

# def get_currency(request):
# 	s = request.path
# 	pat = r'/shop/([\w\d-]+)/'
# 	match = re.search(pat, s)
# 	shop_slug = match.group(1)
# 	currency = Shops.objects.get(slug=shop_slug).currency
# 	return currency
