from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import patterns, include, url
from shoplist.views import *
from shop.views import *
import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    ('^$', shoplist),
    url(r'^shop/(?P<slug>[\w\d]+)/$', shop_main, name='shop_main'),
    url(r'^shop/(?P<slug>[-A-Za-z0-9_]+)/categories/(?P<category_slug>[-A-Za-z0-9_]+)$', category, name='category'),
    url(r'^shop/(?P<slug>[-A-Za-z0-9_]+)/products/(?P<product_slug>[-A-Za-z0-9_]+)$', product, name='product'),
    (r'^cart/', include('cart.urls')),
    url(r'^cart/checkout/', 'cart.views.checkout')
)
urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes':True}),
)
