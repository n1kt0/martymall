from django.contrib import admin
from shoplist.models import Categories, Shops

class CategoriesAdmin(admin.ModelAdmin):
    list_display = ('name', 'category_alias')
    search_fields = ('name', 'category_alias')

    #auto-alias for category name
    prepopulated_fields = {'category_alias' : ('name',)}

class ShopsAdmin(admin.ModelAdmin):
    list_display = ('title', 'category_alias', 'website')
    search_fields = ('title', 'category_alias', 'website')

    # sets up slug to be generated from shops name
    prepopulated_fields = {'slug' : ('title',)}

admin.site.register(Categories, CategoriesAdmin)
admin.site.register(Shops, ShopsAdmin)

