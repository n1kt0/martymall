# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from shoplist.models import *

def shoplist(request):
    categories = Categories.objects.filter()
    shops = Shops.objects.filter()
    return render_to_response('marty/index.html', RequestContext(request, {'categories':categories, 'shops':shops}))