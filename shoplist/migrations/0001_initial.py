# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Categories'
        db.create_table('shoplist_categories', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('category_alias', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('shoplist', ['Categories'])

        # Adding model 'Shops'
        db.create_table('shoplist_shops', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('category_alias', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['shoplist.Categories'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal('shoplist', ['Shops'])


    def backwards(self, orm):
        # Deleting model 'Categories'
        db.delete_table('shoplist_categories')

        # Deleting model 'Shops'
        db.delete_table('shoplist_shops')


    models = {
        'shoplist.categories': {
            'Meta': {'object_name': 'Categories'},
            'category_alias': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'shoplist.shops': {
            'Meta': {'object_name': 'Shops'},
            'category_alias': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['shoplist.Categories']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['shoplist']