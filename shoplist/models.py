from django.db import models
import os

class Categories(models.Model):
    name =  models.CharField(max_length=50)
    category_alias = models.CharField(max_length=50)
    
    class Meta:
        verbose_name_plural = 'Categories'
    
    def __unicode__(self):
        return self.category_alias
        
class Shops(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(("Slug"), help_text=("Used for URLs"), blank=True, null=True)
    description = models.CharField(max_length=300)
    category_alias = models.ForeignKey(Categories)
    image = models.ImageField(upload_to='shops_logo')
    website = models.CharField(max_length=50)
    has_sale = models.BooleanField(("Has sales"), blank=True)
    currency = models.CharField(("Currency"), default="USD", max_length=5)
    
    class Meta:
        verbose_name_plural = 'Shops'
    
    def __unicode__(self):
        return self.slug